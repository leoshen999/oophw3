class POOArticle
{
	public static final int MAXEVAL=1000;
	public int ID;
	public String title;
	public String author;
	public String content;
	public int evcount;
	public int evnum;
	public String[] evmess;
	
	public POOArticle(int i,String t,String a,String c)
	{
		evmess=new String[MAXEVAL];
		evcount=evnum=0;
		ID=i;
		title=t;
		author=a;
		content=c;
	}
	
	public int push(String str)
	{
		if(evnum>=MAXEVAL)return 0;
		evmess[evnum]="Push  "+str;
		evnum++;
		evcount++;
		return 1;
	}
	public int boo(String str)
	{
		if(evnum>=MAXEVAL)return 0;
		evmess[evnum]="Boo   "+str;
		evnum++;
		evcount--;
		return 1;
	}
	public int arrow(String str)
	{
		if(evnum>=MAXEVAL)return 0;
		evmess[evnum]="Arrow "+str;
		evnum++;
		return 1;
	}
	public void show()
	{
		System.out.println("ID: "+ID);
		System.out.println("Title: "+title);
		System.out.println("Author: "+author);
		System.out.println("content:\n"+content);
		System.out.println("evaluation count: "+evcount);
		for(int i=0;i<evnum;i++)
			System.out.println(evmess[i]);
	}
	public void list()
	{
		System.out.println("ID: "+ID);
		System.out.println("Title: "+title);
		System.out.println("Author: "+author);
		System.out.println("evaluation count: "+evcount);
	}
}