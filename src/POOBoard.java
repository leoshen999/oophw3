class POOBoard
{
	public String name;
	
	public int cnt;
	public POOArticle[] article;
	
	public POOBoard(String str)
	{
		name=str;
		cnt=0;
		article=new POOArticle[1000];
	}
	public int add(POOArticle art)
	{
		if(cnt==1000)return 0;
		article[cnt]=art;
		cnt++;
		return 1;
	}
	public int del(int n)
	{
		if(article[n]==null)
			return 0;
		article[n]=null;
		return 1;
	}
	public int move(int src,int dst)
	{
		if(src>=cnt||dst>=cnt||(article[dst]==null&&article[src]==null))return -1;
		POOArticle art;
		art=article[dst];
		article[dst]=article[src];
		article[src]=art;
		return 1;
	}
	public int length()
	{
		int len=0;
		for(int i=0;i<cnt;i++)
			if(article[i]!=null)len++;
		return len;
	}
	public void show()
	{
		for(int i=0;i<cnt;i++)
			if(article[i]!=null)
			{
				System.out.printf("%3d ",i);
				System.out.println(article[i].title);
			}
	}
}