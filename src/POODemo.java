import java.util.Scanner;

class DemoStart
{
	public String user;
	
	public POOBoard[] board;
	public int bor_cnt;
	public POODirectory dir;
	
	public String command;
	private Scanner scanner;
	
	
	public void in_article(POOArticle now)
	{
		while(true)
		{
			System.out.print("\nYou're in article \""+now.title+"\". ");
			System.out.println("What do you want to do?");
			System.out.println("[1]Push [2]Boo [3]Arrow [4]show the content [5]List the imformation [6]Go back");
			
			command=scanner.nextLine();
			int com = Integer.parseInt(command);
			if(com==1)
			{
				System.out.print("Enter the message:");
				command=scanner.nextLine();
				if(now.push(user+": "+command)==0)System.out.println("Can't push the article");
			}
			else if(com==2)
			{
				System.out.print("Enter the message:");
				command=scanner.nextLine();
				if(now.boo(user+": "+command)==0)System.out.println("Can't boo the article");
			}
			else if(com==3)
			{
				System.out.print("Enter the message:");
				command=scanner.nextLine();
				if(now.arrow(user+": "+command)==0)System.out.println("Can't arrow the article");
			}
			else if(com==4)
			{
				now.show();
			}
			else if(com==5)
			{
				now.list();
			}
			else if(com==6)
				return;
			else
				System.out.println("Command Error");
		}
	}
	
	public void in_bor(POOBoard now)
	{
		while(true)
		{
			System.out.print("\nYou're in board \""+now.name+"\". ");
			System.out.println("What do you want to do?");
			System.out.print("[1]Go to another board [2]Add an article [3]Delete an article [4]Move an article");
			System.out.println(" [5]Show all the article [6]Go to an article [7]Go back");
			
			command=scanner.nextLine();
			int com = Integer.parseInt(command);
			if(com==1)
			{
				System.out.print("Enter the name:");
				command=scanner.nextLine();
				for(int i=0;i<bor_cnt;i++)
					if(board[i].name.equals(command))
					{
						now=board[i];
						break;
					}
					else if(i==bor_cnt-1)
						System.out.println("Can't find the board");
			}
			else if(com==2)
			{
				String title;
				String content="";
				
				System.out.print("Enter the title:");
				title=scanner.nextLine();
				System.out.println("Enter the content: (enter a line \"/q\" to finish)");
				while(true)
				{
					String str=scanner.nextLine();
					if(str.equals("/q"))break;
					content+=str;
					content+="\n";
				}
				POOArticle newart=new POOArticle(now.cnt,title,user,content);
				if(now.add(newart)==0)System.out.println("Can't add the article");
			}
			else if(com==3)
			{
				System.out.print("Enter the ID:");
				command=scanner.nextLine();
				com = Integer.parseInt(command);
				if(now.del(com)==0)System.out.println("Can't delete the article");
			}
			else if(com==4)
			{
				int dst,src;
				System.out.print("Enter the source ID:");
				command=scanner.nextLine();
				src = Integer.parseInt(command);
				System.out.print("Enter the destination ID:");
				command=scanner.nextLine();
				dst = Integer.parseInt(command);
				if(now.move(src,dst)==0)System.out.println("Can't move the article");
			}
			else if(com==5)
			{
				now.show();
			}
			else if(com==6)
			{
				System.out.print("Enter the ID:");
				command=scanner.nextLine();
				com=Integer.parseInt(command);
				if(now.article[com]!=null)
					in_article(now.article[com]);
				else
					System.out.println("Can't find the article");
			}
			else if(com==7)
				return;
			else
				System.out.println("Command Error");
		}
	}
	
	public void in_dir(POODirectory now)
	{
		while(true)
		{
			System.out.print("\nYou're in directory \""+now.name+"\". ");
			System.out.println("What do you want to do?");
			System.out.print("[1]Enter the board/directory [2]Create a directory [3]Add a board [4]Add a splitting line [5]Delete an item");
			System.out.println(" [6]Move an item [7]Show all the item [8]Go back");
			
			command=scanner.nextLine();
			int com = Integer.parseInt(command);
			if(com==1)
			{
				System.out.print("Enter the ID:");
				command=scanner.nextLine();
				com=Integer.parseInt(command);
				if(now.dir[com]!=null)
					in_dir(now.dir[com]);
				else if(now.bor[com]!=null)
					in_bor(now.bor[com]);
				else
				System.out.println("Can't find the item");
			}
			else if(com==2)
			{
				System.out.print("Enter the name:");
				command=scanner.nextLine();
				POODirectory newdir=new POODirectory(command);
				if(now.add(newdir)==0)System.out.println("Can't create a directory");
			}
			else if(com==3)
			{
				System.out.print("Enter the name:");
				command=scanner.nextLine();
				for(int i=0;i<bor_cnt;i++)
					if(board[i].name.equals(command))
					{
						if(now.add(board[i])==0)System.out.println("Can't add a board");
						break;
					}
					else if(i==bor_cnt-1)
						System.out.println("Can't find the board");
			}
			else if(com==4)
			{
				if(now.add_split()==0)System.out.println("Can't add a splitting line");
			}
			else if(com==5)
			{
				System.out.print("Enter the ID:");
				command=scanner.nextLine();
				com = Integer.parseInt(command);
				if(now.del(com)==0)System.out.println("Can't delete the item");
			}
			else if(com==6)
			{
				int dst,src;
				System.out.print("Enter the source ID:");
				command=scanner.nextLine();
				src = Integer.parseInt(command);
				System.out.print("Enter the destination ID:");
				command=scanner.nextLine();
				dst = Integer.parseInt(command);
				if(now.move(src,dst)==0)System.out.println("Can't move the item");
			}
			else if(com==7)
			{
				now.show();
			}
			else if(com==8)
				return;
			else
				System.out.println("Command Error");
		}
	}
	
	public void in_lobby()
	{
		while(true)
		{
			System.out.print("\nYou're in lobby now. ");
			System.out.println("What do you want to do?");
			System.out.println("[1]Go to my favorite [2]Show all the boards [3]Go to a board [4]Quit");
			
			command=scanner.nextLine();
			int com = Integer.parseInt(command);
			if(com==1)
			{
				in_dir(dir);
			}
			else if(com==2)
			{
				for(int i=0;i<bor_cnt;i++)
					System.out.println(board[i].name);
			}
			else if(com==3)
			{
				System.out.print("Insert the board name:");
				command=scanner.nextLine();
				for(int i=0;i<bor_cnt;i++)
					if(board[i].name.equals(command))
					{
						in_bor(board[i]);
						break;
					}
					else if(i==bor_cnt-1)
						System.out.println("Can't find the board");
			}
			else if(com==4)
			{
				System.out.println("Good Bye~~");
				return;
			}
			else
				System.out.println("Command Error");
		}
	}
	
	public void start()
	{
		board=new POOBoard[1000];
		dir=new POODirectory("MyFavorite");
		scanner = new Scanner(System.in);
		board[0]=new POOBoard("NTU");
		board[1]=new POOBoard("Gossiping");
		board[2]=new POOBoard("Baseball");
		board[3]=new POOBoard("sex");
		board[4]=new POOBoard("joke");
		bor_cnt=5;
		
		
		System.out.print("Please enter your name: ");
		user=scanner.nextLine();
		in_lobby();
	}
}
class POODemo
{
	public static void main(String[] args)
	{
		DemoStart st=new DemoStart();
		st.start();
	}
}