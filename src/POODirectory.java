class POODirectory
{
	public String name;
	
	public POODirectory[] dir;
	public POOBoard[] bor;
	public int[] lin;
	public int cnt;
	
	public POODirectory(String str)
	{
		name=str;
		dir=new POODirectory[1000];
		bor=new POOBoard[1000];
		lin=new int[1000];
		cnt=0;
	}
	
	
	public int add(POOBoard board)
	{
		if(cnt>=1000)return 0;
		bor[cnt]=board;
		cnt++;
		return 1;
	}
	public int add(POODirectory directory)
	{
		if(cnt>=1000)return 0;
		dir[cnt]=directory;
		cnt++;
		return 1;
	}
	public int add_split()
	{
		if(cnt>=1000)return 0;
		lin[cnt]=1;
		cnt++;
		return 1;
	}
	public int del(int n)
	{
		if(n>=cnt)return 0;
		if(dir[n]==null&&bor[n]==null&&lin[n]==0)return 0;
		dir[n]=null;
		bor[n]=null;
		lin[n]=0;
		return 1;
	}
	public int move(int src,int dst)
	{
		if(src>=cnt||dst>=cnt)return 0;
		if((dir[src]==null&&bor[src]==null&&lin[src]==0)&&(dir[dst]==null&&bor[dst]==null&&lin[dst]==0))return 0;
		
		POODirectory temp1;
		temp1=dir[src];
		dir[src]=dir[dst];
		dir[dst]=temp1;
		
		POOBoard temp2;
		temp2=bor[src];
		bor[src]=bor[dst];
		bor[dst]=temp2;
		
		int temp3;
		temp3=lin[src];
		lin[src]=lin[dst];
		lin[dst]=temp3;
		
		return 1;
	}
	public int length()
	{
		int len=0;
		for(int i=0;i<cnt;i++)
			if(dir[i]!=null||bor[i]!=null||lin[i]!=0)
				len++;
		return len;
	}
	public void show()
	{
		for(int i=0;i<cnt;i++)
			if(dir[i]!=null)
			{
				System.out.printf("%3d Directory: ",i);
				System.out.println(dir[i].name);
			}
			else if(bor[i]!=null)
			{
				System.out.printf("%3d Board    : ",i);
				System.out.println(bor[i].name);
			}
			else if(lin[i]!=0)
			{
				System.out.printf("%3d ",i);
				System.out.println("-----------------");
			}
	}
	
	
	
	
	
}